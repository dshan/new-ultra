module Spree
  class ProductCategory < ActiveRecord::Base
    acts_as_paranoid

    extend Spree::RandomRecord

    # associations
    has_many :products
    has_many :subcategories, :foreign_key => 'parent_id', :class_name => 'Spree::ProductCategory'
    belongs_to :parent, :class_name => 'Spree::ProductCategory'
  
    # validation
    validates :name,  presence: true
    validates_format_of :name, :with => /\A(^[a-zA-ZàáâäãåąćęèéêëìíîïłńòóôöõøùúûüÿýżźñçčšžÀÁÂÄÃÅĄĆĘÈÉÊËÌÍÎÏŁŃÒÓÔÖÕØÙÚÛÜŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$)\Z/i

    # scopes
    scope :top_level, -> { where(:parent_id => nil)  }
    scope :bottom_level, -> { where("parent_id IS NOT NULL") }
    scope :with_parent, ->(parent) { where(:parent_id => parent.id) }

    def is_parent?
      parent_id.nil?
    end
  end
end
