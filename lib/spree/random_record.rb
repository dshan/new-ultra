# Mixin for an active record class so that a random record or records can be picked.
# The records are unique.
module Spree
  module RandomRecord 
    def get_random
      self.get_multiple_random(1).first
    end
  
    def get_multiple_random(count)
      self.limit(count).order("RANDOM()")
    end
  end
end
