class AddParentIdToProductCategories < ActiveRecord::Migration
  def change
    add_column :spree_product_categories, :parent_id, :integer
  end
end
