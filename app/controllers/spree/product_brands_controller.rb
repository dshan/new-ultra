module Spree
  class ProductBrandsController < Spree::StoreController
    before_filter :set_brand
    before_filter :set_page_controls

    def show
    end

  private
    def set_brand
      @brand = Spree::ProductBrand.find_by_name!(params[:id].titleize) if params[:id]
    end

    def set_page_controls
      set_left_nav
      set_breadcrumb
      set_product_display
      set_bestsellers
    end

    def set_left_nav
      @helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :brand })
    end

    def set_product_display
      if @brand
        all_products = Spree::Product.get_products_featured_first 7, nil, @brand
        @main_product = all_products.slice(0..0).first if all_products.any?
        @top_six_products = all_products.slice(1..7)
      end
    end

    def set_breadcrumb
      if @brand
        breadcrumb_helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create @brand
        @breadcrumb_items = breadcrumb_helper.breadcrumb_items
      end
    end

    def set_bestsellers
      if @brand
        @show_bestsellers = true
        @bestsellers = Spree::Product.get_products_featured_first 5, nil, @brand
      end
    end
  end
end
