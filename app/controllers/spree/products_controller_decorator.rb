Spree::ProductsController.class_eval do
  before_filter :set_page_controls
  
private
  def set_page_controls
    set_breadcrumb
    set_left_nav
    set_bestsellers
    set_also_bought_items
  end

  def set_breadcrumb
    if @product
      helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create @product
      @breadcrumb_items = helper.breadcrumb_items
    end
  end

  def set_left_nav
    @helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :product, :product => @product })
  end

  def set_bestsellers
    @show_bestsellers = true
    @bestsellers = Spree::Product.get_multiple_random(6)
  end

  def set_also_bought_items
    @also_bought = Spree::Product.get_multiple_random(6)
  end
end
