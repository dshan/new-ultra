module Spree
  module LeftNavHelper
    class BaseLeftNavHelper# {{{
      attr_reader :show

      def initialize(params = nil)
        @show = true
      end
    end# }}}

    class CategoriesLeftNavHelper < BaseLeftNavHelper# {{{
      attr_reader :show_subcategories
      attr_reader :categories

      def initialize(params = nil)
        super
        @show_subcategories = true
        set_category(params)
      end

      def self.create(params)
        return CategoriesLeftNavHelper.new(params) if (params[:type] == :category)
      end

      def set_category(params)
        if params[:category]
          if params[:category].parent
            @categories = Array.new(1, params[:category].parent)
          else
            @categories = Array.new(1, params[:category])
          end
        end
      end
    end# }}}

    class HomepageLeftNavHelper < CategoriesLeftNavHelper# {{{
      def initialize(params = nil)
        @show_subcategories = false
        @categories = Spree::ProductCategory.top_level
        @show = true
      end

      def self.create(params)
        return HomepageLeftNavHelper.new if (params[:type] == :homepage)
      end
    end# }}}
    
    class ProductLeftNavHelper < CategoriesLeftNavHelper # {{{
      def initialize(params = nil)
        if params[:product] && params[:product].product_category
          super({ :type => :category, :category => params[:product].product_category }) 
        end
      end

      def self.create(params)
        return ProductLeftNavHelper.new(params) if (params[:type] == :product)
      end
    end# }}}

    class BrandLeftNavHelper < HomepageLeftNavHelper# {{{
      def initialize(params = nil)
        super
      end

      def self.create(params)# {{{
        return BrandLeftNavHelper.new(params) if (params[:type] == :brand)    
      end# }}}
    end# }}}

    class LeftNavFactory
      def self.create(params)
        helper ||= HomepageLeftNavHelper.create params
        helper ||= CategoriesLeftNavHelper.create params
        helper ||= ProductLeftNavHelper.create params
        helper ||= BrandLeftNavHelper.create params
        helper
      end
    end
  end
end
