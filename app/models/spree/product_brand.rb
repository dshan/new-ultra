module Spree
  class ProductBrand < ActiveRecord::Base
    acts_as_paranoid

    extend Spree::RandomRecord

    # associations
    has_many :products

    # validation
    validates :name,  presence: true
    validates_format_of :name, :with => /\A(^[a-zA-ZàáâäãåąćęèéêëìíîïłńòóôöõøùúûüÿýżźñçčšžÀÁÂÄÃÅĄĆĘÈÉÊËÌÍÎÏŁŃÒÓÔÖÕØÙÚÛÜŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$)\Z/i

    #def to_param
      #name.parameterize
    #end
  end
end
