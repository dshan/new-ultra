module Spree
  class ProductCategoriesController < Spree::StoreController
    before_filter :set_category
    before_filter :set_parent_category
    before_filter :set_page_controls

    def show
    end

  private
    def set_category
      @category = Spree::ProductCategory.find_by_name!(params[:id].titleize) if params[:id]
    end

    def set_parent_category
      @parent_category = @category.parent if @category
    end

    def set_page_controls
      set_left_nav
      set_breadcrumb
      set_product_display
      set_bestsellers
    end

    def set_left_nav
      @helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :category, :category => @category })
    end

    def set_product_display
      if @category
        categories = get_products_to_display_categories 
        all_products = Spree::Product.get_products_featured_first 7, categories
        @main_product = all_products.slice(0..0).first if all_products.any?
        @top_six_products = all_products.slice(1..7)
      end
    end

    def set_breadcrumb
      if @category
        breadcrumb_helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create @category
        @breadcrumb_items = breadcrumb_helper.breadcrumb_items
      end
    end

    def set_bestsellers
      if @category
        @show_bestsellers = true
        categories = get_products_to_display_categories
        @bestsellers = Spree::Product.get_products_featured_first 5, categories
      end
    end

    def get_products_to_display_categories 
      @parent_category ? [@category] : @category.subcategories
    end
  end
end
