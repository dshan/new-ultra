require 'spec_helper'

describe Spree::ProductCategoriesController do
  context 'get index' do# {{{
    context '#cat not in database' do# {{{
      it { expect{ spree_get :show, :id => "Accessories" }.to raise_error }
    end# }}}
  end# }}}

  describe '.set_category' do# {{{
    let(:category) { FactoryGirl.create(:category) }

    before(:each) do
      spree_get :show, :id => category.name.downcase
    end

    it '@category should be assigned' do
      assigns[:category].should eq category
    end
  end# }}}

  describe '.set_main_designers' do# {{{
    let(:category) { FactoryGirl.create(:category_with_products) }

    context '#cat name passed in' do# {{{
      before(:each) do
        category.name  = "Accessories"
        category.save!

        spree_get :show, { :id => "Accessories" }
      end

      describe '@show_main_designers' do
        it { assigns[:show_main_designers].should be_true }
      end

      describe '@show_subcategories' do
        it { assigns[:show_subcategories].should be_true }
      end

      context '#cat name passed as param' do# {{{
        it '@categories should be assigned correctly' do
          assigns[:categories].should have(1).item
          assigns[:categories].first.name.should eq "Accessories"
        end
      end# }}}
    end# }}}

    context '#no cat name passed in' do# {{{
      before(:each) do
        spree_get :show
      end

      describe '@categories' do
        it { assigns[:categories].should be_nil }
      end
    end# }}}

    context '#subcategory passed in' do# {{{
      let(:parent) { FactoryGirl.create(:category) }

      before(:each) do
        category.parent = parent
        category.save!

        spree_get :show, :id => category.name.downcase
      end

      it '@categories should be assigned as the parent category' do
        assigns[:categories].should have(1).item
        assigns[:categories].first.should eq parent
      end
    end# }}}
  end# }}}

  describe '.set_parent_category' do# {{{
    let (:category) { FactoryGirl.create(:category) }

    context '#no parent' do# {{{
      before(:each) do
        spree_get :show, :id => category.name.downcase
      end
       
      describe '@parent_category' do
        it { assigns[:parent_category].should be nil }
      end
    end# }}}
    
    context '#has parent' do# {{{
      let(:parent) { FactoryGirl.create(:category) }

      before(:each) do
        category.parent = parent
        category.save!
        category.reload

        spree_get :show, :id => category.name.downcase
      end

      it '@parent_category should be set correctly' do
        assigns[:parent_category].should eq category.parent 
      end
    end# }}}
  end# }}}

  describe '.set_breadcrumb' do# {{{
    let(:category) { FactoryGirl.create(:category) }

    context '#no parent' do
      before(:each) do
        spree_get :show, :id => category.name.downcase
      end

      it '@breadcrumb_items should be assigned correclty' do
        assigns[:breadcrumb_items].should have(1).item
      end
    end
    
    context '#has parent' do
      let(:parent) { FactoryGirl.create(:category) }

      before(:each) do
        category.parent = parent
        category.save!

        spree_get :show, :id => category.name.downcase
      end

      it '@breadcrumb_items should have parent category and child category' do
        assigns[:breadcrumb_items].should have(2).items
      end
    end
  end# }}}

  #describe '.create_breadcrumb_item' do# {{{
    #let(:category) { FactoryGirl.create(:category) }
    #let(:controller) { Spree::ProductCategoriesController.new }

    #context '#parameter not of type Spree::ProductCategory ' do
      #it { controller.send(:create_breadcrumb_item, "").should be nil }
    #end

    #context '# is of type Spree::ProductCategory' do
      #it 'should return hash with name and url set correctly' do
        #result = controller.send(:create_breadcrumb_item, category)
        #result[:name].should eq category.name
        #result[:url].should eq "/category/#{ category.name.downcase }"
      #end
    #end
  #end# }}}

  describe '.set_product_display' do# {{{
    let(:parent_category) { FactoryGirl.create(:category) }
    let(:child_categories) { FactoryGirl.create_list(:category_with_products, 3, products_count: 3,
                                                     amount_featured: 1) }

    before(:each) do
      child_categories.each do |category|
        category.parent = parent_category
        category.save!
      end
    end

    context '#pass in name of parent category' do# {{{
      before(:each) do
        spree_get :show, :id => parent_category.name.downcase
      end

      it 'should have 6 products from parent categorys children' do
        assigns[:top_six_products].should have(6).items
      end
    end# }}}

    context '#pass in name of subcategory' do# {{{
      before(:each) do
        spree_get :show, :id => child_categories.first.name.downcase
      end

      it '@main_product should have first product from subcategory' do
        assigns[:main_product].is_a?(Spree::Product).should be_true
      end

      it '@top_six_products should have remaining products only from subcategory' do
        assigns[:top_six_products].should have(2).items
      end
    end# }}}
  end# }}}

  describe '.set_bestsellers' do# {{{
    context '#create parent and category with 5 products' do
      let(:parent) { FactoryGirl.create(:category) }
      let(:category) { FactoryGirl.create(:category_with_products, products_count: 5) }

      before(:each) do
        category.parent = parent
        category.save!
      end

      context '#category set' do# {{{
        before(:each) do
          spree_get :show, :id => category.name.downcase
        end

        describe '@show_bestsellers' do
          it { assigns[:show_bestsellers].should be_true }
        end

        describe '@bestsellers' do
          it { assigns[:bestsellers].should have(5).items }
        end
      end# }}}

      context '#category not set' do# {{{
        before(:each) do
          spree_get :show
        end

        describe '@show_bestsellers' do
          it { assigns[:show_bestsellers].should be_false }
        end

        describe '@bestsellers' do
          it { assigns[:bestsellers].should be_nil }
        end
      end# }}}
    end
  end# }}}
  
  describe '.get_item_breadcrumb_path' do
    let(:controller) { Spree::ProductCategoriesController.new }
  end
end
