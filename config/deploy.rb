
set :application, 'newultra'
set :deploy_user, 'deployer'
set :repo_url, 'https://dshan@bitbucket.org/dshan/new-ultra.git'
#https://dshan@bitbucket.org/dshan/new-ultra.git

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/newultra'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{
  config/database.yml
  config/initializers/secret_token.rb
}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# what specs should be run before deployment is allowed to
# continue, see lib/capistrano/tasks/run_tests.cap
set :tests, []

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# RVM setup
set :rvm_type, :user
set :rvm_ruby_version, '2.0.0-p247'

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set(:config_files, %w(
  nginx.conf
  database.yml
  initializers/secret_token.rb
  unicorn_init.sh
  unicorn.rb
  log_rotation
))

# which config files should be made executable after copying
# by deploy:setup_config
set(:executable_config_files, %w(
  unicorn_init.sh
))

# Enable SSL
#set :enable_ssl, true

# files which need to be symlinked to other parts of the
# filesystem. For example nginx virtualhosts, log rotation
# init scripts etc.
#
# These files have been manually simlinked because of various
# permission annoyances
set(:symlinks, [
  #{
    #source: "nginx.conf",
    #link: "/etc/nginx/sites-enabled/#{fetch(:full_app_name)}"
  #},
  #{
    #source: "unicorn_init.sh",
    #link: "/etc/init.d/unicorn_#{fetch(:full_app_name)}"
  #}
  #,
  #{
    #source: "log_rotation",
    #link: "/etc/logrotate.d/#{fetch(:full_app_name)}"
  #},
])


#namespace :deploy do
  ## make sure we're deploying what we think we're deploying
  ##before :deploy, "deploy:check_revision"

  #desc 'Restart application'
  #task :restart do
    #on roles(:app), in: :sequence, wait: 5 do
      ## Your restart mechanism here, for example:
      ## execute :touch, release_path.join('tmp/restart.txt')
    #end
  #end

  #after :publishing, :restart

  #after :restart, :clear_cache do
    #on roles(:web), in: :groups, limit: 3, wait: 10 do
      ## Here we can do anything such as:
      ## within release_path do
      ##   execute :rake, 'cache:clear'
      ## end
    #end
  #end

#end


# this:
# http://www.capistranorb.com/documentation/getting-started/flow/
# is worth reading for a quick overview of what tasks are called
# and when for `cap stage deploy`

namespace :deploy do
  # make sure we're deploying what we think we're deploying
  before :deploy, "deploy:check_revision"
  # only allow a deploy with passing tests to deployed
  before :deploy, "deploy:run_tests"
  # compile assets locally then rsync
  #after 'deploy:symlink:shared', 'deploy:compile_assets_locally'
  after :finishing, 'deploy:cleanup'

  # remove the default nginx configuration as it will tend
  # to conflict with our configs.
  before 'deploy:setup_config', 'nginx:remove_default_vhost'

  # reload nginx to it will pick up any modified vhosts from
  # setup_config
  after 'deploy:setup_config', 'nginx:reload'

  # Restart monit so it will pick up any monit configurations
  # we've added
  #after 'deploy:setup_config', 'monit:restart'

  # As of Capistrano 3.1, the `deploy:restart` task is not called
  # automatically.
  after 'deploy:publishing', 'deploy:restart'
end


