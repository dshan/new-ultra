class AddIsFeaturedToProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_featured, :boolean
  end
end
