class AddProductBrandToProducts < ActiveRecord::Migration
  def change
    change_table :spree_products do |t|
      t.references :product_brand
    end
  end
end
