module Spree
  module Admin
    class ProductCategoriesController < ResourceController
      #belongs_to 'spree/product_category'
      before_filter :load_data

      def collection
        super.order(:name)
      end

      def index
        session[:return_to] = request.url 
        respond_with(@collection)
      end

      def edit
        enable_dropdown
        super
      end

      def new
        enable_dropdown
        super
      end

      protected

      def load_data
        @parents = Spree::ProductCategory.top_level.includes(:subcategories).order(:name)
      end

      def enable_dropdown
        @dropdown_disabled = @object.is_parent? && @object.subcategories.any?
      end 
    end
  end
end
