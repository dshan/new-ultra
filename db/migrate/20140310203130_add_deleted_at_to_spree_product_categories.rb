class AddDeletedAtToSpreeProductCategories < ActiveRecord::Migration
  def change
    add_column :spree_product_categories, :deleted_at, :datetime
  end
end
