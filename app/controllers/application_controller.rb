class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_left_column
  before_filter :is_admin
  #before_filter :is_logged_in

  def set_left_column
    @show_left_column = true unless (self.class.name == "Spree::OrdersController" || self.class.name == "Spree::CheckoutController")
  end

  def is_admin
    user = try_spree_current_user
    @is_admin = user.nil? ? false : user.admin?
  end

  #def is_logged_in
    #user = try_spree_current_user
    #@logged_in = user.nil? ? false : user.login?
  #end
end
