# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

categories = Spree::ProductCategory.create([
  #{ :name => 'Accessories' },
  { :name => 'Bags' },
  { :name => 'Clothing' },
  #{ :name => 'Cosmetics' },
  #{ :name => 'Gear Jerseys' },
  #{ :name => 'Jeweleries' },
  { :name => 'Shoes' }
  #,
  #{ :name => 'Sports Caps' },
  #{ :name => 'Watches' }# }}}
])

#accessories = Spree::ProductCategory.where(:name => 'Accessories').first# {{{
clothing = Spree::ProductCategory.where(:name => 'Clothing').first# }}}
bags = Spree::ProductCategory.where(:name => 'Bags').first
#cosmetics = Spree::ProductCategory.where(:name => 'Cosmetics').first# {{{
#gear_jerseys = Spree::ProductCategory.where(:name => 'Gear Jerseys').first
#jeweleries = Spree::ProductCategory.where(:name => 'Jeweleries').first
shoes = Spree::ProductCategory.where(:name => 'Shoes' ).first
#sports_caps = Spree::ProductCategory.where(:name => 'Sports Caps').first
#watches = Spree::ProductCategory.where(:name => 'Watches').first# }}}

subcategories = Spree::ProductCategory.create([
  #{ :name => 'Sunglasses', :parent_id => accessories.id },# {{{
  #{ :name => 'Belts', :parent_id => accessories.id },
  #{ :name => 'Ties', :parent_id => accessories.id },
  #{ :name => 'Polarised Sunglasses', :parent_id => accessories.id },
  { :name => 'Bathrobe', :parent_id => clothing.id },
  { :name => 'Tracksuit Bottoms', :parent_id => clothing.id },
  #{ :name => 'Hoodies', :parent_id => clothing.id },
  { :name => 'Short T', :parent_id => clothing.id },# }}}
  { :name => 'Shoulder Bag', :parent_id => bags.id },
  { :name => 'Handbag', :parent_id => bags.id },
  { :name => 'Ipad Bag', :parent_id => bags.id },
  { :name => 'Laptop Bag', :parent_id => bags.id },
  # {{{
  #{ :name => 'Lipstick', :parent_id => cosmetics.id },
  #{ :name => 'Perfume', :parent_id => cosmetics.id },
  #{ :name => 'Make Up', :parent_id => cosmetics.id },
  #{ :name => 'Nail Varnish', :parent_id => cosmetics.id },
  #{ :name => 'Rings', :parent_id => jeweleries.id },
  #{ :name => 'Necklaces', :parent_id => jeweleries.id },
  #{ :name => 'Bracelets', :parent_id => jeweleries.id },
  #{ :name => 'Earrings', :parent_id => jeweleries.id },
  { :name => 'Slippers', :parent_id => shoes.id }
  #,
  #{ :name => 'Trainers', :parent_id => shoes.id },
  #{ :name => 'Boots', :parent_id => shoes.id },
  #{ :name => 'Sports shoes', :parent_id => shoes.id },
  #{ :name => 'Sports', :parent_id => watches.id },
  #{ :name => 'Gold', :parent_id => watches.id },
  #{ :name => 'Jewelled', :parent_id => watches.id },
  #{ :name => 'Bracelet', :parent_id => watches.id },
  #{ :name => 'Golf Cap', :parent_id => sports_caps.id },
  #{ :name => 'Beanie', :parent_id => sports_caps.id },
  #{ :name => 'Flatcap', :parent_id => sports_caps.id },
  #{ :name => 'Visor', :parent_id => sports_caps.id },
  #{ :name => 'Plain Jersey', :parent_id => gear_jerseys.id },
  #{ :name => 'Patterned', :parent_id => gear_jerseys.id },
  #{ :name => 'Baggie Jersey', :parent_id => gear_jerseys.id },
  #{ :name => 'Smart Jersey', :parent_id => gear_jerseys.id }# }}}
])

#sunglasses = Spree::ProductCategory.find_by_name!("Sunglasses")# {{{
#belts = Spree::ProductCategory.find_by_name!("Belts")
#ties = Spree::ProductCategory.find_by_name!("Ties")
#polarised = Spree::ProductCategory.find_by_name!("Polarised Sunglasses") 
#bikini = Spree::ProductCategory.find_by_name!("Bikini")
#jeans = Spree::ProductCategory.find_by_name!("Jeans")
#hoodies = Spree::ProductCategory.find_by_name!("Hoodies")
#short_t = Spree::ProductCategory.find_by_name!("Short T")# }}}
shoulder_bag = Spree::ProductCategory.find_by_name!("Shoulder Bag")
handbag = Spree::ProductCategory.find_by_name!("Handbag")
ipad_bag = Spree::ProductCategory.find_by_name!("Ipad Bag")
laptop_bag = Spree::ProductCategory.find_by_name!("Laptop Bag")
#lipstick = Spree::ProductCategory.find_by_name!("Lipstick")# {{{
#perfume = Spree::ProductCategory.find_by_name!("Perfume")
#make_up = Spree::ProductCategory.find_by_name!("Make Up")
#bracelets = Spree::ProductCategory.find_by_name!("Bracelets")
#earrings = Spree::ProductCategory.find_by_name!("Earrings")
#trainers = Spree::ProductCategory.find_by_name!("Trainers")
#watches_sport = Spree::ProductCategory.find_by_name!("Sports")
#plain_jersey = Spree::ProductCategory.find_by_name!("Plain Jersey")# }}}

brands = Spree::ProductBrand.create([
  { :name => 'New Ultra' },
  # {{{
  #{ :name => 'Fendi' },
  #{ :name => 'Prada' },
  #{ :name => 'Armani' },
  #{ :name => 'Chanel' },
  { :name => 'Lv' },
  { :name => 'Gucci' }
  #{ :name => 'Rolex' },
  #{ :name => 'Coach' }# }}}
])

#armani = Spree::ProductBrand.where(:name => 'Armani').first# {{{
#prada = Spree::ProductBrand.where(:name => 'Prada').first
#chanel = Spree::ProductBrand.where(:name => 'Chanel').first# }}}
new_ultra = Spree::ProductBrand.where(:name => 'New Ultra').first
#fendi = Spree::ProductBrand.where(:name => 'Fendi').first# {{{
gucci = Spree::ProductBrand.where(:name => 'Gucci').first
#coach = Spree::ProductBrand.where(:name => 'Coach').first
lv = Spree::ProductBrand.where(:name => 'Lv').first# }}}

Spree::Sample.load_sample("tax_categories")
Spree::Sample.load_sample("shipping_categories")
Spree::Sample.load_sample("option_values")
Spree::Sample.load_sample("shipping_methods")
#Spree::Sample.load_sample("products")# {{{
#Spree::Sample.load_sample("variants")
#Spree::Sample.load_sample("assets")# }}}

clothing_tax_cat = Spree::TaxCategory.find_by_name!("Clothing")
shipping_category = Spree::ShippingCategory.find_by_name!("Default")

products = [
  { 
    :name => "New Ultra BAG - NO 1",
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => '<p>This bag features a layered mesh centre panel which creates a moving kaleidoscopic effect as it hits the light.</p>
    <p>Framed with high quality durable calf leather, the contours are highlighted by padded decor insets.</p>
    <p>The hard wearing body conscious leather strap is interchangeable with different basic styles and other fitted signature shoulder straps.</p>',
    :product_brand => new_ultra,
    :product_category => shoulder_bag,
    :is_featured => true
  },
  {
    :name => "New Ultra BAG - NO 2",
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "
    <p><strong>Description</strong></p>
    <p>The stunning optical effect of the laminated Italian leather is the main feature of this bag. Its powdered metallic sheen reveals a spectrum of colours when it's brought into the light.</p>
    <p>Silver and rose toned mesh centre panels, padded decor insets, inside pocket with zip closure and fitted signature shoulder strap.</p>
    <p><strong>Details</strong></p> 
    <p>Laminated Holographic Leather / Mesh / Inside pocket with zip closure Dimensions - The bag measures 41cm from top to bottom. The 
    opening measures 27cm leaving sufficient room for carrying laptops and large Tablets.</p>",
    :product_brand => new_ultra,
    :product_category => shoulder_bag,
    :is_featured => true
  },
  #{# {{{
    #:name => 'Armani Large Fabric Shopping Bag with Logo',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'The size shown for this item is the size indicated by the designer on the label.',
    #:product_brand => armani,
    #:product_category => shoulder_bag
  #},
  #{
    #:name => 'Armani Top Handle',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'The size shown for this item is the size indicated by the designer on the label.',
    #:product_brand => armani,
    #:product_category => handbag
  #},
  #{
    #:name => 'Armani Laptop Bag',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'The size shown for this item is the size indicated by the designer on the label.',
    #:product_brand => armani,
    #:product_category => laptop_bag
  #},
  #{
    #:name => 'Armani Jeans J4 Black iPad Case 06V78',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Armani Jeans accessories collection.',
    #:product_brand => armani,
    #:product_category => jeans
  #},
  #{
    #:name => 'Armani Sunglasses EA9809/S - 5R1JJ',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Branded glass case Emporio Armani.',
    #:product_brand => armani,
    #:product_category => sunglasses
  #},
  #{
    #:name => 'Armani Belt115040AR',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Giorgio Armani Mens Brown Leather Belts
    #Width of strap 1.3" inches
    #Length of strap 45" inches ( Can cut down )
    #Belt Fit Waistline : 28"-40" inches
    #1 inches = 2.54 Centimeter
    #Condition : New Product as same Picture show
    #** We can make size fit for you require please email to me again. **',
    #:product_brand => armani,
    #:product_category => belts
  #},
  #{
    #:name => 'Armani Tie 4_14 30_758229',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Model: 758229',
    #:product_brand => armani,
    #:product_category => ties
  #},
  #{
    #:name => 'Armani Jeans Extra Slim Fit T-Shirt - Navy',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Armani Jeans Red Logo T-Shirt. This Armani Jeans Logo T-Shirt 
    #is a simple classic tee featuring a graphic print of the Armani logo on the front.',
    #:product_brand => armani,
    #:product_category => short_t
  #},
  #{
    #:name => 'Armani Jeans Two Tone Slim Fit Brown Hoodie',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => '
        #Two Tone Hoodie by Armani Jeans
        #Ribbed cuff and waistband
        #Hooded
        #50% Wool, 25% Cotton, 25% Plyamide
        #Slim Fit
        #Print at the back
        #U6W38 KL DD',
    #:product_brand => armani,
    #:product_category => hoodies
  #},
  #{
    #:name => 'Armani Jeans 06J31 Regular Fit Light Wash Jeans',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "
        #AJ J21 Regular Fit Jeans
        #5 Pocket Style
        #Logo on back pocket
        #Button Fly
        #Comfort Fabric
        #98% Cotton 2 % Elastane
        #06J31 2Q 15",
    #:product_brand => armani,
    #:product_category => jeans
  #},
  #{
    #:name => '2013 new Fendi handbags Pillow bag FD9106 dark green with white',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Size; W27 x H20 x D14 cm
    #Product ID: FD9106
    #Color: dark green with white
    #2013 new Fendi handbags Pillow bag FD9106 dark green with white Come with serial numbers, 
    #Fendi authenticity card, Fendi dust bag and Fendi care booklet",
    #:product_brand => fendi,
    #:product_category => handbag
  #},
  #{
    #:name => 'Fendi Baguette crystal and perspex shoulder bag',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "The Fendi 'Baguette' is an iconic design. Recreated in blue leather and 
    #embellished with meticulously placed Perspex tiles and tiny crystals, this eye-catching 
    #style is a true conversation starter. ",
    #:product_brand => fendi,
    #:product_category => shoulder_bag
  #},
  #{
    #:name => 'Fendi iPad Case',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Tufted and textured leathers, tapestry, furs and skins in hard frames with 
    #handles were carried down the Milan runway in Fendi’s 2012/13 Autumn Winter collection.",
    #:product_brand => fendi,
    #:product_category => ipad_bag
  #},
  #{
    #:name => 'Fendi Laptop Bag',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Tufted and textured leathers, tapestry, furs and skins in hard frames with 
    #handles were carried down the Milan runway in Fendi’s 2012/13 Autumn Winter collection.",
    #:product_brand => fendi,
    #:product_category => laptop_bag
  #},
  #{
    #:name => 'Fendi Perfume',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "The first perfume for women by Fendi is a chypre floral fragrance with woody 
    #and spicy notes. Ideal harmony is created of intoxicating flowers, fresh spices, soothing 
    #woody notes and gentle musk. The base notes are: ylang-ylang, rose, jasmine, iris, carnation, 
    #nutmeg, patchouli, vetiver, sandalwood, cedar, amber, oak moss and musk. Fendi was launched in 1985. ",
    #:product_brand => fendi,
    #:product_category => perfume
  #},
  #{
    #:name => 'Fendi Chunk Cuff Bracelet',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Black nappa leather chunky cuff bracelet from Fendi featuring an embossed gold-tone pin 
    #buckle fastening and stud detailing.",
    #:product_brand => fendi,
    #:product_category => bracelets
  #},
  #{
    #:name => 'Fendi Earrings',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "These solid Fendi earrings have me intrigued even though they look like they should have 
    #come out of 2001: a space odyssey. They look very forward and very sturdy. Very nice. ",
    #:product_brand => fendi,
    #:product_category => earrings
  #},
  #{
    #:name => 'Fendi Monogram Hightop Trainers',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "After a fairly positive reaction to the Fendi monogram TI$A sneakers, we thought we’d 
    #point your attention to a decent pair from the label itself. The “Active Logo” high tops feature a stitched 
    #leather monogram upper with a leather covered toe cap and strap detail. The look certainly has a bit of an 
    #80s vintage feel, similar to MCM and old Gucci patterns.",
    #:product_brand => fendi,
    #:product_category => trainers
  #},
  #{
    #:name => 'Fendi Analog Watch',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "All Fendi watches feature ETA Swiss movements and are water resistant to 100 feet. Each 
    #gold-tone case has minimum 10 micron 18-karat-gold plating with individual serial numbers engraved on the 
    #case back.",
    #:product_brand => fendi,
    #:product_category => watches_sport 
  #},
  #{
    #:name => 'Fendi Grey Jersey',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Anthracite grey T-shirt made of stretch jersey. Crew neck. Long sleeves. Embroidered Fendi 
    #signature on the chest.",
    #:product_brand => fendi,
    #:product_category => plain_jersey
  #},
  #{
    #:name => 'Fendi Polarised',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Gleaming metal logo design brands the temples of Italian-crafted sunglasses styled with 
    #polarized lenses for glare reduction and full sun protection. Acetate. By Fendi; made in Italy.",
    #:product_brand => fendi,
    #:product_category => polarised
  #},
  #{
    #:name => 'Chanel CH5210Q C6173B 57 Sunglasses',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "An incredibly sophisticated and glamorous pair of oversized Chanel sunglasses, this ladies style features a square-shaped silky black toned frame and arms. The arms also boast a demure black and gold bow feature displaying the Chanel logo, and the lenses are smoke toned.",
    #:product_brand => chanel,
    #:product_category => sunglasses 
  #},
  #{
    #:name => 'Chanel CH5215Q C6173B 57 Sunglasses',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "An ultra striking pair of Chanel sunglasses for women, this oversized cat-eye inspired design features a striped patterned brown frame that draws to a point at the upper outer corners, creating the winged finish. The arms incorporate luxe gold weaved detailing, mimicking the look of the iconic Chanel 2.55 handbag strap. The intertwining Chanel logo is displayed adjacent to the arm stems and the lenses are brown with a graduated finish.",
    #:product_brand => chanel,
    #:product_category => sunglasses 
  #},
  #{
    #:name => 'Chanel CH5205 C8883F 58 Sunglasses',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "An incredibly sophisticated and glamorous pair of oversized Chanel sunglasses, this ladies style features a square-shaped silky black toned frame and arms. The arms also boast a demure black and gold bow feature displaying the Chanel logo, and the lenses are smoke toned.",
    #:product_brand => chanel,
    #:product_category => sunglasses 
  #},
  #{
    #:name => 'Fendi Sunglasses - FS5298R/001/56-15',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Black On Square Black Gradient Lens",
    #:product_brand => fendi,
    #:product_category => sunglasses 
  #},
  #{
    #:name => 'Gucci Sunglasses 4200 Brown Havana Brown Gradient WNK CC',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Gucci Sunglasses 4200 Brown Havana Brown Gradient WNK CC this visor style frame is elegant and cool and the visor lens not only gives excellent protection for your eyes but also for the skin under the large lens which helps prevent sun damage to the face.",
    #:product_brand => gucci,
    #:product_category => sunglasses 
  #},
  #{
    #:name => 'Gucci Sunglasses GG1013/S',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "The Unisex Gucci Sunglasses GG1013/S (GG1013/S 51N PT 56) is available to buy from Shade Station. It features a frame which is black, green and red with the gucci logo and has a lens which is graduated grey. The GG1013/S 51N PT 56 comes with an official Gucci Sunglasses 1 year guarantee.",
    #:product_brand => gucci,
    #:product_category => sunglasses 
  #},
  {
    :name => 'Gucci Stirrup GG fabric Shoulder Bag 296856 White ',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => '
    <p>Gucci Stirrup GG fabric Shoulder Bag 296856 White</p>
     
    <ul>
      <li>beige/ebony original GG fabric with White leather trim</li>
      <li>palladium hardware</li>
      <li>natural cotton linen lining</li>
      <li>Made in Italy</li>
      <li>double handles with 8.7" drop and small spur hardware</li>
      <li>strap closure with snap and large spur detail</li>
      <li>interior zip and smart phone pockets</li>
    </ul>
     
    <p>Size: W35 x H29 x D18cm(1" = 2.54cm)</p>',
    :product_brand => gucci,
    :product_category => shoulder_bag 
  },
  #{
    #:name => 'Coach Signature Convertible Shoulder Bag',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Two leather straps, one is perfect to wear on shoulder, the second can be worn across body style with adjustable strap
    
    #Signature cross body bag with khaki jacquard material and genuine leather trim (water and stain resistant)
    
    #Top zip closure with large zipper pull, beautiful COACH "C" signature print
    
    #Silver hardware, interior is fully lined
    
    #Measure 12.5" x 11" x 2"
    
    #Zip pocket, slip pocket and D-ring for keys',
    #:product_brand => gucci,
    #:product_category => shoulder_bag 
  #}, }}}
  {
    :name => 'Louis Vuitton handbag LV Handbag LV bags leather bag',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => " We supply more than 15000 styles brand new handbags, such as LV, Coach, Chanel, Prada, Gucci, Burberry, Hermes, ED Hardly, CA, etc. The more you buy, the more discount. ",
    :product_brand => lv,
    :product_category => handbag
  },
  {
    :name => 'Louis Vuitton Taylor Eyelet Leather Satchel Handbag (C510)',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>Inside Zip, Cell Phone and Multifunction Pockets</p>
        <p>Zip-Top Closure, Fabric Lining</p>
            <p>Longer Strap for Shoulder Wear</p>",
    :product_brand => lv,
    :product_category => handbag
  },
  {
    :name => 'Gucci Messenger Bag Park Color Block Leather Gold (C901)',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<ul>
    <li>Inside zip and multifunction pockets</li>
    <li>Snap closure, fabric lining</li>
    <li>Outside open pocket</li>
    <li>Adjustable strap for shoulder or crossbody wear<li>",
    :product_brand => lv,
    :product_category => handbag
  },
  #{{{ { 
    #:name => 'Prada Evening bag Sequenced Evening Designer Handbags (PR6009)',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => "Fully Lined Satin Interior",
    #:product_brand => coach,
    #:product_category => handbag
  #}, }}}
  { 
    :name => 'Gucci iPad Case',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>It was only a matter of time before Gucci rolled out their iPad cases. And just in time too, with Christmas just round the corner with barely two months to go. Especially in Singapore where the X’mas decorations will be going up along Orchard Road this time of year faster than you can say Santa Claus ten times.</p>",
    :product_brand => gucci,
    :product_category => ipad_bag 
  },
  { 
    :name => 'New Ultra iPad 2 Case - Matt Black',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => " </p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Matt Calf Leather)</p>", :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  },
  { 
    :name => 'New Ultra iPad 2 Case - Embossed Black',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Embossed black Leather)</p>",
    :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  },
  { 
    :name => 'New Ultra iPad 2 Case - Embossed White',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Embossed black Leather)</p>",
    :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  },
  { 
    :name => 'New Ultra iPad 2 Case - Green',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Embossed black Leather)</p>",
    :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  },
  { 
    :name => 'New Ultra iPad 2 Case - Blue',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Embossed black Leather)</p>",
    :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  },
  { 
    :name => 'New Ultra iPad 2 Case - Coloured Handles',
    :tax_category => clothing_tax_cat,
    :shipping_category => shipping_category,
    :price => 10.99,
    :eur_price => 15,
    :description => "<p>These Ipad cases showcase our signature amorphous design styled into a bracelet. By using the functional bracelet you adorn your hand and wrist with beautifully contoured lines whilst keeping your precious technology safe. (Embossed black Leather)</p>",
    :product_brand => new_ultra,
    :product_category => ipad_bag,
    :is_featured => true
  }
  #,# {{{
  #{ 
    #:name => 'Coach Signature Stripe iPad Case F61761 - Khaki/Mahogany',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => '
        #Signature coated canvas with fabric trim
        #Easel functionality
        #Circular cutout for camera
        #8" (L) x 10" (H)
        #This is a signature product',
    #:product_brand => coach,
    #:product_category => ipad_bag 
  #},
  #{ 
    #:name => 'Coach Discount Earring Online Outlet Online Cheapest#1738',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Get these Coach accessories and so many wonderful more, and live it up this summer in true Coach style!Choose our Coach accessories you can easily enjoy the feeling of fashion and sweetly.These planners are available in a variety of colors for Coach Caps to make our lives easier and fashioner, in the world of business.',
    #:product_brand => coach,
    #:product_category => earrings 
  #},
  #{ 
    #:name => 'Louis Vuitton Earrings Jewelry Online Cheapest ID:ve7019',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Model: Louis Vuitton Earrings Jewelry Online Cheapest ID:ve7019',
    #:product_brand => lv,
    #:product_category => earrings 
  #},
  #{ 
    #:name => 'Chanel Double CC Logo Crystal Stud Earrings (A37272)',
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Chanel Double CC Logo Crystal Stud Earrings (A37272)
    #Size : 1.3x1.7cm
    #Comes with box, dustbag, paperbag & a copy of receipt. ',
    #:product_brand => chanel,
    #:product_category => earrings 
  #},
  #{ 
    #:name => "Vintage Chanel 1990's Rope Textured Logo Drop Earrings",
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Vintage Chanel earrings sourced by Susan Caplan Vintage. Featuring a drop circle design with an interlinking logo, gold plated, textured twist style trim and a clip on fastening.',
    #:product_brand => chanel,
    #:product_category => earrings 
  #},
  #{ 
    #:name => "Prada rose crystal clip-on earrings",
    #:tax_category => clothing_tax_cat,
    #:shipping_category => shipping_category,
    #:price => 10.99,
    #:eur_price => 15,
    #:description => 'Prada rose crystal clip-on earrings',
    #:product_brand => prada,
    #:product_category => earrings 
  #}# }}}
]

default_attrs = {
  :description => Faker::Lorem.paragraph,
  :available_on => Time.zone.now
}

products.each do |product_attrs|
  eur_price = product_attrs.delete(:eur_price)
  Spree::Config[:currency] = "USD"

  default_shipping_category = Spree::ShippingCategory.find_by_name!("Default")
  product = Spree::Product.create!(default_attrs.merge(product_attrs))
  Spree::Config[:currency] = "EUR"
  product.reload
  product.price = eur_price
  product.shipping_category = default_shipping_category
  product.save!
end

productz = {}
productz[:no_1] = Spree::Product.find_by_name!("New Ultra BAG - NO 1")
productz[:no_2] = Spree::Product.find_by_name!("New Ultra BAG - NO 2")
#productz[:armani_shopping_bag_1] = Spree::Product.find_by_name!("Armani Large Fabric Shopping Bag with Logo")# {{{
#productz[:armani_top_handle] = Spree::Product.find_by_name!("Armani Top Handle")
#productz[:armani_laptop_bag] = Spree::Product.find_by_name!("Armani Laptop Bag")
#productz[:armani_ipad] = Spree::Product.find_by_name!("Armani Jeans J4 Black iPad Case 06V78")
#productz[:armani_sunglasses] = Spree::Product.find_by_name!("Armani Sunglasses EA9809/S - 5R1JJ")
#productz[:armani_belt] = Spree::Product.find_by_name!("Armani Belt115040AR")
#productz[:armani_tie] = Spree::Product.find_by_name!("Armani Tie 4_14 30_758229")
#productz[:armani_short_t] = Spree::Product.find_by_name!("Armani Jeans Extra Slim Fit T-Shirt - Navy")
#productz[:armani_hoodie] = Spree::Product.find_by_name!("Armani Jeans Two Tone Slim Fit Brown Hoodie")
#productz[:armani_jeans] = Spree::Product.find_by_name!("Armani Jeans 06J31 Regular Fit Light Wash Jeans")
#productz[:fendi_handbag] = Spree::Product.find_by_name!("2013 new Fendi handbags Pillow bag FD9106 dark green with white")
#productz[:fendi_shoulderbag] = Spree::Product.find_by_name!("Fendi Baguette crystal and perspex shoulder bag")
#productz[:fendi_ipad_case] = Spree::Product.find_by_name!("Fendi iPad Case")
#productz[:fendi_laptop_bag] = Spree::Product.find_by_name!("Fendi Laptop Bag")
#productz[:fendi_perfume] = Spree::Product.find_by_name!("Fendi Perfume")
#productz[:fendi_bracelet] = Spree::Product.find_by_name!("Fendi Chunk Cuff Bracelet")
#productz[:fendi_earrings] = Spree::Product.find_by_name!("Fendi Earrings")
#productz[:fendi_trainers] = Spree::Product.find_by_name!("Fendi Monogram Hightop Trainers")
#productz[:fendi_analog_watch] = Spree::Product.find_by_name!("Fendi Analog Watch")
#productz[:fendi_jersey] = Spree::Product.find_by_name!("Fendi Grey Jersey")
#productz[:fendi_sunglasses_1] = Spree::Product.find_by_name!("Fendi Sunglasses - FS5298R/001/56-15")
#productz[:chanel_sungalasses_1] = Spree::Product.find_by_name!("Chanel CH5210Q C6173B 57 Sunglasses")
#productz[:chanel_sungalasses_2] = Spree::Product.find_by_name!("Chanel CH5215Q C6173B 57 Sunglasses")
#productz[:chanel_sungalasses_3] = Spree::Product.find_by_name!("Chanel CH5205 C8883F 58 Sunglasses")
#productz[:gucci_sunglasses_1] = Spree::Product.find_by_name!("Gucci Sunglasses 4200 Brown Havana Brown Gradient WNK CC")
productz[:gucci_stirrup] = Spree::Product.find_by_name!("Gucci Stirrup GG fabric Shoulder Bag 296856 White ")
#productz[:gucci_shoulder_bag_1] = Spree::Product.find_by_name!("Gucci Stirrup GG fabric Shoulder Bag 296856 White ")
#productz[:coach_shoulder_bag] = Spree::Product.find_by_name!("Coach Signature Convertible Shoulder Bag")
#productz[:lv_handbag_1] = Spree::Product.find_by_name!("Louis Vuitton handbag LV Handbag LV bags leather bag")
#productz[:coach_handbag_1] = Spree::Product.find_by_name!("Coach Taylor Eyelet Leather Satchel Handbag (C510)")
#productz[:coach_handbag_2] = Spree::Product.find_by_name!("Coach Messenger Bag Park Color Block Leather Pink / Brown Crossbody (C901)")
#productz[:prada_handbag] = Spree::Product.find_by_name!("Prada Evening bag Sequenced Evening Designer Handbags (PR6009)")
#productz[:prada_ipad] = Spree::Product.find_by_name!("Prada iPad Case")# }}}
productz[:newultra_ipad] = Spree::Product.find_by_name!("New Ultra iPad 2 Case - Matt Black")
productz[:newultra_ipad_2] = Spree::Product.find_by_name!("New Ultra iPad 2 Case - Embossed Black")
productz[:newultra_ipad_3] = Spree::Product.find_by_name!("New Ultra iPad 2 Case - Embossed White")
#productz[:coach_ipad] = Spree::Product.find_by_name!("Coach Signature Stripe iPad Case F61761 - Khaki/Mahogany")# {{{
#productz[:coach_earrings] = Spree::Product.find_by_name!("Coach Discount Earring Online Outlet Online Cheapest#1738")
#productz[:lv_earrings] = Spree::Product.find_by_name!("Louis Vuitton Earrings Jewelry Online Cheapest ID:ve7019")
#productz[:chanel_earrings] = Spree::Product.find_by_name!("Chanel Double CC Logo Crystal Stud Earrings (A37272)")
#productz[:chanel_earrings_2] = Spree::Product.find_by_name!("Vintage Chanel 1990's Rope Textured Logo Drop Earrings")
#productz[:prada_earrings] = Spree::Product.find_by_name!("Prada rose crystal clip-on earrings")# }}}

def image(name, type="jpeg")
  images_path = Pathname.new(File.dirname(__FILE__)) + "images"
  path = images_path + "#{name}.#{type}"
  return false if !File.exist?(path)
  File.open(path)
end

images = {
  productz[:no_1].master => [
    {
      :attachment => image("no_1_front", "jpg")
    },
    { 
      :attachment => image("no_1_back", "jpg")
    }
  ],
  productz[:no_2].master => [
    { 
      :attachment => image("no_2_front", "jpg")
    },
    { 
      :attachment => image("no_2_back", "jpg")
    }
  ],
  productz[:gucci_stirrup].master => [
    { 
      :attachment => image("gucci 1", "jpg")
    }
  ],
  productz[:newultra_ipad].master => [
    { 
      :attachment => image("newultra_ipad_1_1", "png")
    },
    { 
      :attachment => image("newultra_ipad_1_2", "jpg")
    },
    { 
      :attachment => image("newultra_ipad_1_3", "jpg")
    }
  ],
  productz[:newultra_ipad_2].master => [
    { 
      :attachment => image("newultra_ipad_2_1", "jpg")
    },
    { 
      :attachment => image("newultra_ipad_2_2", "jpg")
    }
  ],
  productz[:newultra_ipad_3].master => [
    { 
      :attachment => image("newultra_ipad_3_1", "jpg")
    },
    { 
      :attachment => image("newultra_ipad_3_2", "jpg")
    }
  ]
}

images.each do |variant, attachments|
  puts "Loading images for #{variant.name}"
  attachments.each do |attachment|
    variant.images.create!(attachment)
  end
end

location = Spree::StockLocation.first_or_create! name: 'default'
location.active = true
location.country = Spree::Country.where(iso: 'GB').first
location.save!

Spree::Variant.all.each do |variant|
  quantity = (variant.product.product_brand == new_ultra) ? 10 : 0

  variant.stock_items.each do |stock_item|
    Spree::StockMovement.create(:quantity => quantity, :stock_item => stock_item)
  end
end
