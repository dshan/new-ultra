Newultra::Application.routes.draw do
  Spree::Core::Engine.routes.append do
    resources :product_categories, path: '/category', only: [:show]
    resources :product_brands, path: '/brand', only: [:show]

    namespace :admin do
      resources :product_categories
      resources :product_brands
    end
  end

  mount Spree::Core::Engine, :at => '/'

end
