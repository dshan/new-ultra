module Spree
  module PathHelper
    class ProductPathHelper# {{{
      def show_method
        :product_path
      end
      
      def self.create(product)
        return nil unless product.is_a?(Spree::Product)
        ProductPathHelper.new
      end
    end# }}}
  
    class BrandPathHelper# {{{
      def show_method
        :product_brand_path
      end

      def self.create(brand)
        return nil unless brand.is_a? Spree::ProductBrand
        BrandPathHelper.new
      end
    end# }}}
  
    class CategoryPathHelper# {{{
      def show_method
        :product_category_path
      end

      def self.create(category)
        return nil unless category.is_a? Spree::ProductCategory
        CategoryPathHelper.new
      end
    end# }}}
  
    class PathHelperFactory# {{{
      def self.create(item)
        helper ||= PathHelper::ProductPathHelper.create item 
        helper ||= PathHelper::BrandPathHelper.create item 
        helper ||= PathHelper::CategoryPathHelper.create item
        helper
      end
    end# }}}
  end
end
