
Deface::Override.new(:virtual_path => 'spree/admin/products/_form',
  :name => 'add_product_brand_to_product_form',
  :insert_before => "erb[loud]:contains('field_container :shipping_categories')",
  :text => "
    <%= f.field_container :product_brand do %>
      <%= f.label :product_brand_id, Spree.t(:Brand) %>
      <%= f.collection_select(:product_brand_id, @product_brands, :id, :name, { }, { :class => 'select2' }) %>
      <%= f.error_message_on :product_brand %>
    <% end %>
  ")
