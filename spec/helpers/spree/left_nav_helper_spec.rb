require 'spec_helper'

describe 'Spree::LeftNavHelper' do
  let(:category) { FactoryGirl.create(:category) }

  describe 'Spree::LeftNavHelper::LeftNavFactory' do# {{{
    describe 'self.create ' do# {{{
      context '#type is unrecognised' do# {{{
        it 'should return nil' do
          helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :test })
          helper.should be_nil
        end
      end# }}}

      context '#type is :homepage' do# {{{
        it 'should return HomepageLeftNavHelper' do
          helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :homepage })
          helper.should be_kind_of(Spree::LeftNavHelper::HomepageLeftNavHelper)
        end
      end# }}}

      context '#type is :category' do# {{{
        it 'should return CategoriesLeftNavHelper' do
          helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :category })
          helper.should be_kind_of(Spree::LeftNavHelper::CategoriesLeftNavHelper)
        end
      end# }}}

      context '#type is :product' do# {{{
        it 'should return ProductLeftNavHelper' do
          helper = Spree::LeftNavHelper::LeftNavFactory.create({ :type => :product })
          helper.should be_kind_of(Spree::LeftNavHelper::ProductLeftNavHelper)
        end
      end# }}}
    end# }}}
  end# }}}

  describe 'Spree::LeftNavHelper::HomepageLeftNavHelper' do# {{{  
    let(:helper) { Spree::LeftNavHelper::LeftNavFactory.create({ :type => :homepage }) }

    before(:each) do
      FactoryGirl.create(:category_with_subcategories)
    end

    describe 'helper.show' do# {{{
      it { helper.show.should be_true }
    end# }}}

    describe '.show_subcategories' do# {{{
      it { helper.show_subcategories.should be_false }
    end# }}}

    describe '.categories' do# {{{
      it 'should have only top level categories' do
        helper.categories.should have(1).item
        helper.categories.first.should eq Spree::ProductCategory.top_level.first
      end
    end# }}}
  end# }}}

  describe 'Spree::LeftNavHelper::CategoriesLeftNavHelper' do# {{{  
    let(:helper) { Spree::LeftNavHelper::CategoriesLeftNavHelper.create({ :type => :category, :category => category } ) }

    context '#if category' do# {{{
      context '#and parent' do# {{{
        let(:parent) { FactoryGirl.create(:category) }

        before(:each) do
          category.parent = parent
          category.save!
        end

        it 'should set categories to array containing just the parent category' do
          helper.categories.should have(1).item
          helper.categories.first.should eq parent
        end
      end# }}}

      context '#and no parent' do# {{{
        it 'should set category to array context category' do
          helper.categories.should have(1).item
          helper.categories.first.should eq category
        end
      end# }}}
    end# }}}

    context '#if no category' do# {{{
      it 'should set categories to nil' do
        no_cat_helper = Spree::LeftNavHelper::CategoriesLeftNavHelper.create({ :type => :category })
        no_cat_helper.categories.should be_nil
      end
    end# }}}

    describe '.show' do# {{{
      it { helper.show.should be_true }
    end# }}}
  end# }}}

  context 'Spree::LeftNavHelper::ProductLeftNavHelper' do# {{{  
    let(:product) { FactoryGirl.create(:product) }
    let(:helper) { Spree::LeftNavHelper::ProductLeftNavHelper.create({ :type => :product, :product => product }) }

    context '#if has category' do# {{{
      before(:each) do
        product.product_category = category
        product.save!
      end

      it 'should set categories' do
        helper.categories.should have(1).item
        helper.categories.first.should eq category
      end

      describe '.show' do# {{{
        it { helper.show.should be_true }
      end# }}}
    end# }}}

    context '#no category' do# {{{
      it 'shuold set categories as nil ' do
        helper.categories.should be_nil
      end

      describe '.show' do# {{{
        it { helper.show.should be_nil }
      end# }}}
    end# }}}
  end# }}}
end
