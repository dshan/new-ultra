Deface::Override.new(:virtual_path => 'spree/admin/shared/_configuration_menu',
                     :name => 'add_product_brands_and_categories_to_configuration_menu',
                     :insert_after => "erb[loud]:contains('configurations_sidebar_menu_item Spree.t(:general_settings)')",
                     :text => "
                          <%= configurations_sidebar_menu_item Spree.t(:product_categories), admin_product_categories_path %>
                          <%= configurations_sidebar_menu_item Spree.t(:product_brands), admin_product_brands_path %>
")
