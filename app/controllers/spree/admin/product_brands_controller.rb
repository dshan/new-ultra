module Spree
  module Admin
    class ProductBrandsController < ResourceController
      def collection
        super.order(:name)
      end

      def index
        session[:return_to] = request.url 
        respond_with(@collection)
      end
    end
  end
end
