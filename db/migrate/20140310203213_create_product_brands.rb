class CreateProductBrands < ActiveRecord::Migration
  def change
    create_table :spree_product_brands do |t|
      t.string :name
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
