require 'spec_helper'

describe 'Spree::BreadcrumbHelper' do
  let(:product) { FactoryGirl.create(:product) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:category) { FactoryGirl.create(:category) }

  describe 'BreadcrumbHelperFactory' do# {{{
    describe 'self.create' do# {{{
      context '#product' do# {{{
        it 'should return ProductBreadcrumbHelper' do
          helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create product
          helper.is_a?(Spree::BreadcrumbHelper::ProductBreadcrumbHelper).should be_true
        end
      end# }}}

      context '#brand' do# {{{
        it 'should return BrandBreadcrumbHelper' do
          helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create brand
          helper.is_a?(Spree::BreadcrumbHelper::BrandBreadcrumbHelper).should be_true
        end
      end# }}}

      context '#category' do# {{{
        it 'should return a CategoryBreadcrumbHelper' do
          helper = Spree::BreadcrumbHelper::BreadcrumbHelperFactory.create category
          helper.is_a?(Spree::BreadcrumbHelper::CategoryBreadcrumbHelper).should be_true
        end
      end# }}}
    end# }}}
  end# }}}

  describe 'BreadcrumbItem' do# {{{
    let(:breadcrumb_item) { Spree::BreadcrumbHelper::BreadcrumbItem.new(product) }

    describe '.initialize' do# {{{
      it 'should set name correctly' do
        breadcrumb_item.name.should eq product.name
      end

      it 'it should set url_method correctly' do
        breadcrumb_item.url_method.should be :product_path
      end
    end# }}}
  end# }}}

  describe 'BrandBreadcrumbHelper' do# {{{
    describe 'self.create' do# {{{
      context '#brand' do# {{{
        let(:helper) { Spree::BreadcrumbHelper::BrandBreadcrumbHelper.create(brand) }

        it 'should return BrandBreadcrumbHelper ' do
          helper.is_a?(Spree::BreadcrumbHelper::BrandBreadcrumbHelper).should be_true
        end
        
        it 'should set breadcrumb items with 1 item' do
          helper.breadcrumb_items.should have(1).item
        end
      end# }}}

      context '#not brand' do# {{{
        it 'should return nil' do
          helper = Spree::BreadcrumbHelper::BrandBreadcrumbHelper.create product
          helper.should be_nil
        end
      end# }}}
    end# }}}
  end# }}}

  describe 'CategoryBreadcrumbHelper' do# {{{
    describe 'self.create' do# {{{
      context '#category passed in' do# {{{
        let(:helper) { Spree::BreadcrumbHelper::CategoryBreadcrumbHelper.create(category) }

        it 'should return CategoryBreadcrumbHelper' do
          helper.is_a?(Spree::BreadcrumbHelper::CategoryBreadcrumbHelper).should be_true 
        end

        context '#category has parent' do# {{{
          let(:parent) { FactoryGirl.create(:category) }

          it 'should set breadcrumb_items with 2 items' do
            category.parent = parent
            category.save!

            helper.breadcrumb_items.should have(2).items
          end
        end# }}}
        
        context '#category has no parent' do# {{{
          it 'should set breadcrumb_items with 1 item ' do
            helper.breadcrumb_items.should have(1).item
          end
        end# }}}
      end# }}}

      context '#category not passed in' do# {{{
        it 'should return nil' do
          helper = Spree::BreadcrumbHelper::CategoryBreadcrumbHelper.create brand
          helper.should be_nil
        end
      end# }}}
    end# }}}
  end# }}}

  describe 'ProductBreadcrumbHelper' do# {{{
    describe 'self.create' do# {{{
      context '#product passed in' do# {{{ 
        let(:helper) { Spree::BreadcrumbHelper::ProductBreadcrumbHelper.create(product) }

        it 'should return ProductBreadcrumbHelper' do
          helper.is_a?(Spree::BreadcrumbHelper::ProductBreadcrumbHelper).should be_true
        end

        context '#has brand only' do# {{{
          it 'should return 2 items' do
            product.product_brand = brand
            product.save!
            helper.breadcrumb_items.should have(2).items
          end
        end# }}}

        context '#has category' do# {{{
          it 'should return 2 items' do
            product.product_category = category
            product.save!
            helper.breadcrumb_items.should have(2).items
          end
        end# }}}

        describe 'has category with parent' do# {{{
          let(:parent) { FactoryGirl.create(:category) }

          it 'should return 3 items' do
            category.parent = parent
            category.save!
            product.product_category = category
            product.save!
            helper.breadcrumb_items.should have(3).items
          end
        end# }}}

        context '#has no brand or category' do# {{{
          it 'should return 1 item' do
            helper.breadcrumb_items.should have(1).item
          end
        end# }}}
      end# }}}

      context '#product not passed in' do# {{{
        it 'should return nil' do
          helper = Spree::BreadcrumbHelper::ProductBreadcrumbHelper.create brand
          helper.should be_nil
        end
      end# }}}
    end# }}}
  end# }}}
end
