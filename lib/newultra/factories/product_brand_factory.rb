FactoryGirl.define do
  factory :brand, class: Spree::ProductBrand do
    name { generate(:random_name) }

    factory :brand_with_products do
      ignore do
        products_count 5
        amount_featured 1
      end

      after(:create) do |brand, evaluator|
        create_list(:product, evaluator.products_count, product_brand_id: brand.id)

        brand.products.each_with_index do |product, index|
          if index < evaluator.amount_featured
            product.is_featured = true
            product.save!
          end
        end
      end
    end
  end
end
