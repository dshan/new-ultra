require 'spec_helper'

describe Spree::ProductsController do
  describe '.show' do

  end

  describe '.set_breadcrumb' do
    context '#product not set' do
      before(:each) do
        spree_get :show
      end

      describe '@breadcrumb_items' do
        it { assigns[:breadcrumb_items].should be_nil }
      end
    end

    context '#product set' do
      let (:product) { FactoryGirl.create(:product) }

      before(:each) do
        spree_get :show, :id => product
      end

      describe '@breadcrumb_items' do
        it { assigns[:breadcrumb_items].should_not be_nil }
      end
    end
  end
end
