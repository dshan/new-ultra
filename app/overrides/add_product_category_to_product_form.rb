
Deface::Override.new(:virtual_path => 'spree/admin/products/_form',
  :name => 'add_product_category_to_product_form',
  :insert_before => "erb[loud]:contains('field_container :shipping_categories')",
  :text => "
    <%= f.field_container :product_category do %>
      <%= f.label :product_category_id, Spree.t(:Category) %>
      <%= f.collection_select(:product_category_id, @product_categories, :id, :name, { }, { :class => 'select2' }) %>
      <%= f.error_message_on :product_category %>
    <% end %>
  ")
