require 'spec_helper'

describe Spree::ProductCategory do
  describe '#associations' do
    it { should have_many :products }
    it { should have_many :subcategories }
    it { should belong_to :parent }
  end

  describe '#validation' do
    it { should validate_presence_of :name }
  end

  describe '#scopes' do
    before(:each) do
      @top_level_categories = FactoryGirl.create_list(:category_with_subcategories, 7)
    end

    describe '.top_level' do
      it 'should have 21 categories in total' do
        Spree::ProductCategory.all.should have(28).items 
      end

      it 'should select the 7 top level categories' do
        Spree::ProductCategory.top_level.should have(7).items
        (@top_level_categories.map(&:id) - Spree::ProductCategory.top_level.map(&:id)).should be_empty
      end
    end

    describe '.with_parent' do
      it 'should be able to select all subcategories by parent ' do
        parent = @top_level_categories.first 
        subcategories = Spree::ProductCategory.with_parent parent
        subcategories.each{ |subcategory| subcategory.parent_id.should eq parent.id }
      end
    end
  end
end
