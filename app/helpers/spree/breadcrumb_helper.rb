module Spree
  module BreadcrumbHelper
    class BreadcrumbHelperBase# {{{
      attr_reader :breadcrumb_items

      def initialize(item)
        @breadcrumb_items = Array.new 
      end
    end# }}}

    class BreadcrumbItem# {{{
      attr_reader :name
      attr_reader :url_method
      attr_reader :object

      def initialize(item)
        path_helper = Spree::PathHelper::PathHelperFactory.create item 
        @name = item.name
        @url_method = path_helper.show_method
        @object = item
      end
    end# }}}

    class ProductBreadcrumbHelper < BreadcrumbHelperBase# {{{
      def initialize(product)
        super(product)
        if product.product_category
          @breadcrumb_items << BreadcrumbItem.new(product.product_category.parent) if product.product_category.parent
          @breadcrumb_items << BreadcrumbItem.new(product.product_category)
        end
        @breadcrumb_items << BreadcrumbItem.new(product.product_brand) if product.product_brand
        @breadcrumb_items << BreadcrumbItem.new(product)
      end

      def self.create(product)
        return ProductBreadcrumbHelper.new(product) if product.is_a?(Spree::Product)
      end
    end# }}}

    class CategoryBreadcrumbHelper < BreadcrumbHelperBase# {{{
      def initialize(category)
        super(category) 
        @breadcrumb_items << BreadcrumbItem.new(category.parent) if category.parent
        @breadcrumb_items << BreadcrumbItem.new(category)
      end

      def self.create(category)
        return CategoryBreadcrumbHelper.new(category) if category.is_a?(Spree::ProductCategory)
      end
    end# }}}

    class BrandBreadcrumbHelper < BreadcrumbHelperBase# {{{
      def initialize(brand)
        super(brand)
        @breadcrumb_items << BreadcrumbItem.new(brand)
      end

      def self.create(brand)
        return BrandBreadcrumbHelper.new(brand) if brand.is_a?(Spree::ProductBrand)
      end
    end# }}}

    class BreadcrumbHelperFactory# {{{
      def self.create(item)
        helper ||= ProductBreadcrumbHelper.create item
        helper ||= BrandBreadcrumbHelper.create item
        helper ||= CategoryBreadcrumbHelper.create item
        helper
      end
    end# }}}
  end
end
