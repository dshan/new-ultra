class AddProductCategoryToProducts < ActiveRecord::Migration
  def change
    change_table :spree_products do |t|
      t.references :product_category
    end
  end
end
