# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Newultra::Application.initialize!

# Set up hirb for console
#if $0 == 'irb'
  #require 'hirb'
  #Hirb.enable
#end
