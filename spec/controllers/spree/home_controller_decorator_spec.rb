require 'spec_helper'

describe Spree::HomeController do 
  context "get index" do
    before(:each) do
      spree_get :index 
    end

    describe ".set_main_designers" do# {{{
      describe "@show_main_designers" do
        it { assigns[:show_main_designers].should be_true }
      end

      describe '@show_subcategories' do
        it { assigns[:show_subcategories].should be_false }
      end

      context "#create 3 product categories" do
        before(:each) do
          3.times.map{ FactoryGirl.create(:category) } 
        end

        it "@categories should have 3 items" do
          assigns[:categories].should have(3).items
        end
      end

      context "#create 2 product categories and 6 subcategory" do
        before(:each) do
          2.times.map { FactoryGirl.create(:category_with_subcategories) }
        end

        it '@categories should have 2 items' do
          assigns[:categories].should have(2).items
        end

        it 'each category should have 3 subcategories' do
          assigns[:categories].each do |cat|
            cat.subcategories.should have(3).items
          end
        end
      end
    end# }}}

    describe '.set_bestsellers' do# {{{
      describe "@show_bestsellers" do
        it { assigns[:show_bestsellers].should be_true }
      end
    end# }}}

    describe '.set_left_nav' do# {{{
      describe "@show_nav" do
        it { assigns[:show_nav].should be_false }
      end
    end# }}}

    describe '.set_recently_bought' do# {{{
      context '#create 20 products and 20 states' do
        before(:each) do
          FactoryGirl.create_list(:product, 20)
          FactoryGirl.create_list(:state, 20)
        end

        describe '@show_recently_bought' do
          it { assigns[:show_recently_bought].should be true }
        end

        describe '@recently_bought_products' do
          it { assigns[:recently_bought_products].should have(12).items }
        end

        describe '@states' do
          it { assigns[:states].should have(12).items }
        end
      end
    end# }}}

    describe '.set_hot_keywords' do# {{{
      context '#create category with 5 products and brand with 5 products' do
        before(:each) do
          FactoryGirl.create_list(:category_with_products, 5)
          FactoryGirl.create_list(:brand_with_products, 5)
        end

        describe '@show_hot_keywords' do
          it { assigns[:show_hot_keywords].should be true }
        end

        describe '@hk_categories' do
          it { assigns[:hk_categories].should have(2).items }
        end

        describe '@hk_brands' do
          it { assigns[:hk_brands].should have(5).items }
        end
      end
    end# }}}

    describe '.set_top_six_products' do# {{{
      context '#create "Sunglasses" category with 10 products' do
        let(:category_sunglasses) { FactoryGirl.create(:category_with_products, products_count: 10) }

        before(:each) do
          category_sunglasses.name  = "Sunglasses"
          category_sunglasses.save!
        end

        context '#pass in array containing "Sunglasses" category' do# {{{
          let(:controller) { Spree::HomeController.new }

          before(:each) do
            controller.instance_eval{ set_top_six_products(["Sunglasses"]) } 
          end

          describe '@random_category' do
            it { controller.instance_eval{ @random_category }.name.should eq "Sunglasses" }
          end

          describe '@top_six_products' do
            it { controller.instance_eval{ @top_six_products }.should have(6).items }
          end

          context '#make first 5 products of "Sunglasses" category featured' do# {{{
            before(:each) do
              category_sunglasses.products.each_with_index do |product, index|
                if index < 5
                  product.is_featured = true
                else
                  product.is_featured = false
                end
                product.save!
              end

              controller.instance_eval{ set_top_six_products(["Sunglasses"]) }
            end

            describe '@top_six_products' do
              it { controller.instance_eval{ @top_six_products }.should have(6).items }

              it 'should have 5 featured products' do
                count = 0
                controller.instance_eval{ @top_six_products }.each{ |product| count = count + (product.is_featured ? 1 : 0) }
                count.should eq 5 
              end

              it 'should have 1 non featured product' do
                count = 0
                controller.instance_eval{ @top_six_products }.each{ |product| count = count + (product.is_featured ? 0 : 1) }
                count.should eq 1 
              end
            end
          end# }}}
        end# }}}

        context '#pass in array with category not in database' do# {{{
          let(:controller) { Spree::HomeController.new }

          before(:each) do
            controller.instance_eval{ set_top_six_products(["iPad Bag", "Handbag", "Earrings"]) } 
          end

          describe '@random_category' do
            it { controller.instance_eval{ @random_category }.should be nil }
          end

          describe '@top_six_products' do
            it { controller.instance_eval{ @top_six_products }.should eq Array.new }
          end
        end# }}}
      end
    end# }}}

    describe '.set_breadcrumb' do# {{{
      describe '@breadcrumb_items' do
        it { assigns[:breadcrumb_items].should be nil }
      end
    end# }}}
  end
end
