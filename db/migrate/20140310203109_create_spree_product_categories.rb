class CreateSpreeProductCategories < ActiveRecord::Migration
  def change
    create_table :spree_product_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
