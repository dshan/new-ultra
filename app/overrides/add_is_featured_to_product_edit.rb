Deface::Override.new(:virtual_path => 'spree/admin/products/_form',
  :name => 'add_sale_price_to_product_edit',
  :insert_before => "erb[loud]:contains('field_container :shipping_categories')",
  :text => "
    <%= f.field_container :is_featured do %>
      <%= f.label :is_featured  %>
      <br />
      <%= f.check_box :is_featured, :style => 'width: 10px;' %> 
      <span style='font-size: 9px; padding-left: 5px'>Check this box if you want this product to appear at the top of all controls</span>
      <%= f.error_message_on :is_featured %>
    <% end %>
  ")
