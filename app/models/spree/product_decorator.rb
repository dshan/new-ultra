Spree::Product.class_eval do
  extend Spree::RandomRecord

  # associations
  belongs_to :product_category
  belongs_to :product_brand

  # scopes
  scope :of_brand, ->(brand) { where product_brand_id: brand.id }
  scope :of_category, ->(category) { where product_category_id: category.id }
  scope :of_categories, ->(categories) { where(:product_category_id => categories.map(&:id)) }
  scope :of_featured, ->(featured) { where is_featured: featured }

  after_initialize :post_init

  def post_init
    self.is_featured ||= false
  end

  def self.get_products_featured_first(count, categories = nil, brand = nil)
    if categories
      self.featured_first_categories count, categories
    elsif brand
      self.featured_first_brand count, brand
    end
  end

  private
  def self.featured_first_categories(count, categories)
    featured = self.get_multiple_random(count).of_categories(categories).of_featured(true)
    non_featured = self.get_multiple_random(count).of_categories(categories).of_featured(false)
    (featured + non_featured).slice(0..(count - 1))
  end

  def self.featured_first_brand(count, brand)
    featured = self.get_multiple_random(count).of_brand(brand).of_featured(true)
    non_featured = self.get_multiple_random(count).of_brand(brand).of_featured(false)
    (featured + non_featured).slice(0..(count - 1))
  end
end
