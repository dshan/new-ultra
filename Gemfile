source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.3'

# Use sqlite3 as the database for Active Record
gem 'pg'
gem 'activerecord-postgresql-adapter', '~> 0.0.1'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', '~> 0.12.0', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 3.1.0'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 1.3.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.3.20', require: false
end

group :development, :test do
  gem 'better_errors', '~> 1.0.1'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'pry', '~> 0.9.12.3'
  gem 'pry-rails', '~> 0.3.2'
  gem 'pry-doc', '~> 0.4.6'
  gem 'pry-stack_explorer'
  gem 'hirb', '~> 0.7.1'
  gem 'rspec-rails', '~> 2.14.0'
end

group :test do
  gem 'sqlite3', '~> 1.3.8'
  gem 'simplecov'
  gem 'database_cleaner'
  gem 'factory_girl_rails', '~> 4.3.0'
  gem 'shoulda-matchers', '~> 2.5.0'
  gem 'spork-rails', '~> 4.0.0'
  gem 'memory_test_fix', '~> 0.2.2'
end

group :production, :staging do
  gem 'rails_12factor', '~> 0.0.2'
  gem 'unicorn'
end

gem 'spree', :git => 'https://github.com/spree/spree.git', :branch => '2-1-stable'
gem 'spree_gateway', :git => 'https://github.com/spree/spree_gateway.git', :branch => '2-1-stable'
gem 'spree_auth_devise', :git => 'https://github.com/spree/spree_auth_devise.git', :branch => '2-1-stable'

# Custom extensions
#gem 'spree_wholesalerun_styles', :path => '../spree_extensions/spree_wholesalerun_styles'

gem 'spree_paypal_express', :github => "radar/better_spree_paypal_express", :branch => "2-1-stable"

gem 'capistrano', '~> 3.1.0'
gem 'capistrano-rvm', '~> 0.1.1'
gem 'capistrano-rails', '~> 1.1.0'
gem 'capistrano-bundler', '~> 1.1.2'
