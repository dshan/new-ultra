module Spree
  HomeController.class_eval do
    before_filter :set_page_controls

    def index
      @searcher = build_searcher(params)
      @products = @searcher.retrieve_products
    end

  private
    def set_page_controls
      set_left_nav
      set_main_designers
      set_bestsellers
      set_recently_bought
      set_hot_keywords
      set_top_six
      set_breadcrumb
    end

    def set_main_designers
      @show_main_designers = true
      @show_subcategories = false
      @categories = Spree::ProductCategory.top_level
    end

    def set_bestsellers
      @show_bestsellers = true
      @bestsellers = Spree::Product.get_multiple_random(11)
    end

    def set_left_nav
      @helper = LeftNavHelper::LeftNavFactory.create({ :type => :homepage })
    end

    def set_recently_bought
      @show_recently_bought = true
      @recently_bought_products = Spree::Product.get_multiple_random(12)
      @states = Spree::State.get_multiple_random(12)
    end

    def set_hot_keywords
      @show_hot_keywords = true
      @hk_categories = Spree::ProductCategory.top_level.get_multiple_random(2)
      @hk_brands = Spree::ProductBrand.get_multiple_random(5)
    end

    def set_top_six
      # featured items
      set_top_six_products ["Sunglasses", "Shoulder Bag", "Handbag", "Ipad Bag", "Earrings"]
    end

    def set_top_six_products(category_array)
      @category_name = category_array.sample
      @random_category = Spree::ProductCategory.find_by_name(@category_name)
      categories = @random_category ? [@random_category] : []
      @top_six_products = Spree::Product.get_products_featured_first 6, categories
    end

    def set_breadcrumb
      @breadcrumb_items = nil
    end
  end
end
