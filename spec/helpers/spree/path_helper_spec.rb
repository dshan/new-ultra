require 'spec_helper'

describe Spree::PathHelper do
  let(:product) { FactoryGirl.create(:product) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:category) { FactoryGirl.create(:category) }

  
  context '#PathHelperFactory' do
    context '.create' do# {{{
      context '#product ' do# {{{
        it 'should create ProductPathHelper class' do
          helper = Spree::PathHelper::PathHelperFactory.create product
          helper.is_a?(Spree::PathHelper::ProductPathHelper).should be_true
        end
      end# }}}

      context '#brand' do# {{{
        it 'should create BrandPathHelper' do
          helper = Spree::PathHelper::PathHelperFactory.create brand
          helper.is_a?(Spree::PathHelper::BrandPathHelper).should be_true
        end
      end# }}}

      context '#category' do# {{{
        it 'should create CategoryPathHelper' do
          helper = Spree::PathHelper::PathHelperFactory.create category 
          helper.is_a?(Spree::PathHelper::CategoryPathHelper).should be_true
        end
      end# }}}
    end# }}}
  end

  context '#ProductPathHelper' do# {{{
    describe '.create' do# {{{
      context '#if product' do# {{{
        it 'should return ProductPathHelper' do
          helper = Spree::PathHelper::ProductPathHelper.create(product)
          helper.is_a?(Spree::PathHelper::ProductPathHelper).should be_true
        end
      end# }}}

      context '#if not product' do# {{{
        it 'it should return nil' do
          helper = Spree::PathHelper::ProductPathHelper.create(category)
          helper.should be_nil
        end
      end# }}}
    end# }}}

    describe '.show_method' do# {{{
      it 'should return correct product path method' do
        method = Spree::PathHelper::ProductPathHelper.new  
        method.show_method.should eq :product_path 
      end
    end# }}}
  end# }}}

  describe '#BrandPathHelper' do# {{{
    context '.create ' do
      context '#pass in brand' do
        it 'should return BrandPathHelper ' do
          helper = Spree::PathHelper::BrandPathHelper.create brand
          helper.is_a?(Spree::PathHelper::BrandPathHelper).should be_true
        end
      end

      context '#pass in non brand' do# {{{
        it 'should return nil' do
          helper = Spree::PathHelper::BrandPathHelper.create product
          helper.should be_nil
        end
      end# }}}
    end

    context 'show_method' do# {{{
      it 'should return correct show_method' do
        helper = Spree::PathHelper::BrandPathHelper.new
        helper.show_method.should be :product_brand_path
      end
    end# }}}
  end# }}}

  describe '#CategoryPathHelper' do# {{{
    describe '.create' do# {{{
      context '#pass in category' do# {{{
        it 'should return CategoryPathHelper ' do
          helper = Spree::PathHelper::CategoryPathHelper.create category
          helper.is_a?(Spree::PathHelper::CategoryPathHelper).should be_true
        end
      end# }}}

      context '#pass in non category' do# {{{
        it 'should return nil' do
          helper = Spree::PathHelper::CategoryPathHelper.create brand
          helper.should be_nil
        end
      end# }}}
    end# }}}

    context '.show_method' do# {{{
      it 'should return correct show_method' do
        helper = Spree::PathHelper::CategoryPathHelper.new
        helper.show_method.should be :product_category_path
      end
    end# }}}
  end# }}}
end
