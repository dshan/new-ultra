require 'spec_helper'
require 'pry'

describe Spree::Product do
  describe '#create' do
    let(:product) { FactoryGirl.create(:product) }

    describe '.is_featued' do
      it { product.is_featured.should be_false }
    end
  end

  describe '#associations' do
    it { should belong_to(:product_category) }
    it { should belong_to(:product_brand) }
  end

  describe '#scopes' do
    describe '.of_brand ' do
      let(:brand) { FactoryGirl.create(:brand_with_products) }

      it 'can find products by brand' do
        products_by_brand  = Spree::Product.of_brand(brand)
        products_by_brand.should have(5).items
      end
    end

    describe '.of_category' do
      let(:category) { FactoryGirl.create(:category_with_products) }

      it 'can find products by category' do
        products_by_category = Spree::Product.of_category(category)
        products_by_category.should have(4).items
      end
    end

    describe '.of_categories' do
      let(:categories) { FactoryGirl.create_list(:category_with_products, 5) }
      let(:top_two_categories) { categories.slice(0..2) }

      it 'selects all products of top 2 categories' do
        results = Spree::Product.of_categories(top_two_categories)

        top_two_categories.each do |category| 
          category.products.each{ |product| results.include?(product).should be_true } 
        end
      end
    end

    describe '.of_featured' do
      let(:products) { FactoryGirl.create_list(:product, 5) }

      before(:each) do
        products.each_with_index do |product, index|
          if index < 2
            product.is_featured = true
            product.save!
          end
        end
      end

      it 'can find featured products' do
        Spree::Product.of_featured(true).should have(2).items         
      end
       
      it 'can find non featured products' do
        Spree::Product.of_featured(false).should have(3).items         
      end
    end
  end

  context 'Selecting random records' do
    let(:products) { create_list(:product, 10) }

    before(:each) do
      products.first.reload
      Spree::Product.all.count.should eq 10
    end

    describe '.get_random' do
      it 'is of type product' do
        Spree::Product.get_random.class.name.should eq "Spree::Product" 
      end

      it 'selects single random record' do
        count = 0

        rand_prod_ids = Array.new

        10.times do
          rand_prod_ids << Spree::Product.get_random.id
        end

        rand_prod_ids.uniq.count.should be > 1
      end
    end

    describe '.get_multiple_random' do
      it 'should return collection of type product' do
        Spree::Product.get_multiple_random(5).each do |product| 
          product.class.name.should eq "Spree::Product"
        end
      end

      it 'should return specified number of records' do
        product_ids = Spree::Product.get_multiple_random(8).map(&:id)

        product_ids.count.should eq 8
      end

      it 'all items are unique' do
        product_ids = Spree::Product.get_multiple_random(7).map(&:id)

        product_ids.uniq.count.should eq 7
      end

      it 'calling method twice returns different set of records' do
        first_product_ids = Spree::Product.get_multiple_random(4).map(&:id)
        second_product_ids = Spree::Product.get_multiple_random(4).map(&:id)

        (first_product_ids - second_product_ids).count.should be > 0
      end
    end
  end

  describe '.get_products_featured_first' do
    context '#by category' do
      context '#create featured products and non featured  ' do
        let(:parent_category) { FactoryGirl.create(:category) }
        let(:child_categories) { FactoryGirl.create_list(:category_with_products, 5, products_count: 5, 
                                                    amount_featured: 3) }

        before(:each) do
          child_categories.each do |category|
            category.parent = parent_category
            category.save!
          end
        end

        context '#pass in one category only' do
          let(:results) { Spree::Product.get_products_featured_first 5, [child_categories.first] } 

          it 'should return 5 items in result array' do
            results.should have(5).items
          end

          it 'should return 3 featured products' do
            count = 0
            results.each{ |product| count = count + ((product.is_featured) ? 1 : 0) }

            count.should eq 3
          end

          it 'should return 2 non featured products' do
            count = 0
            results.each{ |product| count = count + ((product.is_featured) ? 0 : 1) }

            count.should eq 2
          end
        end
        
        context '#pass in many categories' do
          let(:results) { Spree::Product.get_products_featured_first 5, child_categories }

          it 'should return 5 items' do
            results.should have(5).items 
          end

          it 'should return all featured items' do
            count = 0
            results.each{ |product| count = count + ((product.is_featured) ? 1 : 0) }

            count.should eq 5
          end
        end
      end
    end

    context '#by brand' do
      context '#create featured products and non featured' do
        let(:brands_with_featured) { FactoryGirl.create_list(:brand_with_products, 5, products_count: 7,
                                                             amount_featured: 3) }

        context '#pass in one brand' do
          let(:results) { Spree::Product.get_products_featured_first 7, nil, brands_with_featured.first }

          it 'should return count amoount of products' do
            results.should have(7).items
          end

          it 'should return all featured products' do
            count = 0
            results.each{ |product| count = count + ((product.is_featured) ? 1 : 0) }

            count.should eq 3
          end

          it 'should return remaining non featured products' do
            count = 0
            results.each{ |product| count = count + ((product.is_featured) ? 0 : 1) }

            count.should eq 4
          end
        end
      end
    end
  end
end
