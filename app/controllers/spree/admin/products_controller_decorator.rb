Spree::Admin::ProductsController.class_eval do
  alias_method :orig_load_data, :load_data
  def load_data
    orig_load_data
    @product_categories = Spree::ProductCategory.bottom_level
    @product_brands = Spree::ProductBrand.all
  end
end
