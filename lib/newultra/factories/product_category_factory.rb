FactoryGirl.define do
  factory :category, class: Spree::ProductCategory do
    name { generate(:random_name) }

    factory :category_with_products do
      ignore do
        products_count 4
        amount_featured 1
      end

      after(:create) do |category, evaluator|
        create_list(:product, evaluator.products_count, product_category_id: category.id)

        category.products.each_with_index do |product, index|
          if index < evaluator.amount_featured
            product.is_featured = true 
            product.save!
          end
        end
      end
    end

    factory :category_with_subcategories do
      ignore do
        subcategories_count 3
      end

      after(:create) do |category, evaluator|
        subcats = create_list(:category, evaluator.subcategories_count, parent: category)
      end
    end
  end
end
